package com.igmi.lines.strategies;

import java.io.File;
import java.io.FileNotFoundException;

public interface FileStatsCounterStrategy {
    int countFileStats(File file) throws FileNotFoundException;
}
