package com.igmi.lines.strategies;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class CodeLinesCountFileStatsStrategy implements FileStatsCounterStrategy {
    private static final String MULTI_COMMENT_START = "/*";
    private static final String MULTI_COMMENT_END = "*/";

    private boolean complexCommentStarted = false;

    @Override
    public int countFileStats(File file) throws FileNotFoundException {
        Scanner scanner = new Scanner(file);
        int counter = 0;
        while (scanner.hasNextLine()) {
            String line = scanner.nextLine().trim();
            if (isComment(line)) {
                continue;
            }
            if (!isEmpty(line)) {
                counter++;
            }
        }

        return counter;
    }

    private boolean isEmpty(String value) {
        return value == null || "".equals(value);
    }

    private boolean isComment(String line) {
        boolean hasSimpleComment = isSimpleComment(line);

        ComplexCommentLineStats complexCommentLineStats = isStartEndComplexComment(line);
        boolean result = complexCommentStarted;
        if (complexCommentStarted) {
            if (complexCommentLineStats.hasCloseComment()) {
                complexCommentStarted = false;
                result = complexCommentLineStats.hasSimpleCommentAfterLastClose() || !complexCommentLineStats.hasSymbolsAfterLastClose();
            } else {
                if (hasSimpleComment) {
                    return hasSimpleComment;
                }
            }
        } else {
            if (hasSimpleComment) {
                return hasSimpleComment;
            }
            if (complexCommentLineStats.hasOpenComment()) {
                complexCommentStarted = true;
                result = !complexCommentLineStats.hasSymbolsBeforeFirstOpen();
            }
        }
        return result;
    }

    private static ComplexCommentLineStats isStartEndComplexComment(String line) {
        int multiCommentStartIndex = line.indexOf(MULTI_COMMENT_START);
        int multiCommentEndIndex = line.indexOf(MULTI_COMMENT_END);
        if (multiCommentStartIndex > -1 || multiCommentEndIndex > -1) {
            boolean isSimpleComment = false;
            boolean isOpen = false;
            boolean isClose = false;
            boolean symbolsAfterLastClose = false;
            boolean symbolsBeforeFirstOpen = false;
            boolean simpleComment = false;
            boolean simpleCommentAfterLastClose = false;
            int symbolsBeforeFirstOpenCounter = 0;

            int index = 0;
            while (index < line.length() - 1) {
                char charAtIndex = line.charAt(index);
                symbolsAfterLastClose = true;
                symbolsBeforeFirstOpenCounter++;
                if (charAtIndex == '"') {
                    isSimpleComment = !isSimpleComment;
                } else if (charAtIndex == '/' && !isSimpleComment) {
                    if (line.charAt(index + 1) == '/') {
                        simpleComment = true;
                        simpleCommentAfterLastClose = true;
                        index++;
                    }
                    if (line.charAt(index + 1) == '*' && !simpleComment) {
                        isOpen = true;
                        isClose = false;
                        if (symbolsBeforeFirstOpenCounter > 1) {
                            symbolsBeforeFirstOpen = true;
                            symbolsBeforeFirstOpenCounter = Integer.MIN_VALUE;
                        }
                        index++;
                    }
                } else if (charAtIndex == '*' && !isSimpleComment) {
                    if (line.charAt(index + 1) == '/') {
                        isClose = !isOpen;
                        isOpen = false;
                        symbolsAfterLastClose = false;
                        simpleCommentAfterLastClose = false;
                        index++;
                    }
                }
                index++;
            }
            return new ComplexCommentLineStats(isOpen, isClose, symbolsBeforeFirstOpen, symbolsAfterLastClose,
                                               simpleCommentAfterLastClose);
        }
        return new ComplexCommentLineStats(false, false);
    }

    private boolean isSimpleComment(String line) {
        return line.startsWith("//");
    }


    private static class ComplexCommentLineStats {
        private final Boolean openComment;
        private final Boolean closeComment;
        private final Boolean symbolsBeforeFirstOpen;
        private final Boolean symbolsAfterLastClose;
        private final Boolean simpleCommentAfterLastClose;

        public ComplexCommentLineStats(final Boolean openComment, final Boolean closeComment,
                                       final Boolean symbolsBeforeFirstOpen,
                                       final Boolean symbolsAfterLastClose,
                                       final Boolean simpleCommentAfterLastClose) {
            this.openComment = openComment;
            this.closeComment = closeComment;
            this.symbolsBeforeFirstOpen = symbolsBeforeFirstOpen;
            this.symbolsAfterLastClose = symbolsAfterLastClose;
            this.simpleCommentAfterLastClose = simpleCommentAfterLastClose;
        }

        public ComplexCommentLineStats(final Boolean openComment, final Boolean closeComment) {
            this.openComment = openComment;
            this.closeComment = closeComment;
            this.symbolsAfterLastClose = false;
            this.symbolsBeforeFirstOpen = false;
            this.simpleCommentAfterLastClose = false;
        }

        public Boolean hasOpenComment() {
            return openComment;
        }

        public Boolean hasCloseComment() {
            return closeComment;
        }

        public Boolean hasSymbolsBeforeFirstOpen() {
            return symbolsBeforeFirstOpen;
        }

        public Boolean hasSymbolsAfterLastClose() {
            return symbolsAfterLastClose;
        }

        public Boolean hasSimpleCommentAfterLastClose() {
            return simpleCommentAfterLastClose;
        }
    }
}
