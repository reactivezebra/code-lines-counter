package com.igmi.lines.service;

import com.igmi.lines.strategies.FileStatsCounterStrategy;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class FileStatsCounter {
    private final FileStatsCounterStrategy fileStatsCounterStrategy;
    private File rootFile;

    public FileStatsCounter(final File rootFile, FileStatsCounterStrategy fileStatsCounterStrategy) {
        this.rootFile = rootFile;
        this.fileStatsCounterStrategy = fileStatsCounterStrategy;
    }

    public FileStatsCounterResult process() throws FileNotFoundException {
        HashMap<String /*filePath*/, FileRecord> linesCount = new HashMap<>();
        int i = countLines(rootFile, 0, linesCount);
        return new FileStatsCounterResult(linesCount, rootFile.getAbsolutePath());
    }

    private int countLines(File file, int depth, HashMap<String, FileRecord> linesCount) throws FileNotFoundException {
        if (file.isDirectory()) {
            File[] files = file.listFiles();
            if (files == null) {
                throw new IllegalArgumentException("Illegal directory:" + file.getAbsolutePath());
            }
            int counter = 0;
            List<String> childNames = new ArrayList<String>();
            for (File childFile : files) {
                counter += countLines(childFile, depth + 1, linesCount);
                childNames.add(childFile.getAbsolutePath());
            }
            linesCount.put(file.getAbsolutePath(), new FileRecord(counter, file.getAbsolutePath(), depth,
                                                                  childNames));
            return counter;
        } else {
            int fileStats = fileStatsCounterStrategy.countFileStats(file);
            linesCount.put(file.getAbsolutePath(),
                           new FileRecord(fileStats, file.getAbsolutePath(), depth));
            return fileStats;
        }
    }


    public static class FileStatsCounterResult {
        private final HashMap<String, FileRecord> linesCount;
        private final String root;

        private FileStatsCounterResult(HashMap<String, FileRecord> linesCount, String root) {
            this.linesCount = linesCount;
            this.root = root;
        }

        public void printResults() {
            printResultsInner(linesCount, root);
        }

        public HashMap<String, FileRecord> getRawResults() {
            return linesCount;
        }

        private void printResultsInner(HashMap<String, FileRecord> linesCount, String root) {
            FileRecord fileRecord = linesCount.get(root);
            printSpaces(fileRecord.getDepth());
            System.out.println(fileRecord.getPath() + " : " + fileRecord.getCount());
            if (fileRecord.getChild().isEmpty()) {
                return;
            }

            for (String fileName : fileRecord.getChild()) {
                printResultsInner(linesCount, fileName);
            }
        }

        private void printSpaces(int amount) {
            //Multiply amount by 2 to beautify output
            System.out.print(IntStream.range(0, amount * 2).boxed().map(v -> " ").collect(Collectors.joining()));
        }

        @Override
        public String toString() {
            return "FileStatsCounterResult{" +
                   "linesCount=" + linesCount +
                   '}';
        }
    }

    public static class FileRecord {
        private final int count;
        private final String path;
        private final int depth;
        private final List<String> child;

        public FileRecord(final int count, final String path, final int depth, final List<String> child) {
            this.count = count;
            this.path = path;
            this.depth = depth;
            this.child = child;
        }

        public FileRecord(final int count, final String path, int depth) {
            this.count = count;
            this.path = path;
            this.depth = depth;
            this.child = new ArrayList<String>();
        }

        public int getCount() {
            return count;
        }

        public String getPath() {
            return path;
        }

        public List<String> getChild() {
            return child;
        }

        public int getDepth() {
            return depth;
        }

        @Override
        public String toString() {
            return "FileRecord{" +
                   "count=" + count +
                   ", path='" + path + '\'' +
                   ", child=" + child +
                   '}';
        }
    }
}