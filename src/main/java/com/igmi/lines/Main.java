package com.igmi.lines;

import com.igmi.lines.service.FileStatsCounter;
import com.igmi.lines.strategies.CodeLinesCountFileStatsStrategy;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) throws FileNotFoundException {
        System.out.println("------- Code Lines Counter -------");
        System.out.println("Please write file path:");
        Scanner scanner = new Scanner(System.in);
        String filePath = scanner.nextLine();
        File rootFile = new File(filePath);
        if (!rootFile.exists()) {
            System.out.println("File not found!");
            return;
        }
        FileStatsCounter.FileStatsCounterResult result = new FileStatsCounter(rootFile,
                                                                              new CodeLinesCountFileStatsStrategy()).process();
        result.printResults();
    }
}
