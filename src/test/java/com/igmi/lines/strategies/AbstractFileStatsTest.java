package com.igmi.lines.strategies;


import org.apache.commons.io.FileUtils;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.TestInstance;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

@TestInstance(value = TestInstance.Lifecycle.PER_CLASS)
public abstract class AbstractFileStatsTest {

    protected static String ROOT_DIRECTORY = "test";

    protected static String FILE_NO_COMMENTS = "test/file5.java";
    protected static String FILE_1_SIMPLE_COMMENT = "test/folder2/file3.java";
    protected static String FILE_1_MULTILINE_COMMENT = "test/folder2/file4.java";
    protected static String FILE_COMPLEX_LINES = "test/folder3/file6.java";


    @BeforeAll
    public void createFilesHierarchy() throws IOException {
        removeFilesHierarchy();
        createFile("test/folder1/file1.java", "1");
        createFile("test/folder1/file2.java", "1\n2");
        createFile("test/folder2/file3.java", "1\n2\n//sss\n3");
        createFile("test/folder2/file4.java", "1\n2\n3\n/*afsafs\nsadasddas*/\n4");
        createFile("test/file5.java", "1\n2\n3\n4\n5");
        createFile("test/folder3/file6.java", "/*****\n" +
                                              "     * This is a test program with 5 lines of code\n" +
                                              "     *  \\/* no nesting allowed!\n" +
                                              "     //*****//***/// Slightly pathological comment ending...\n" +
                                              "  public class Hello {\n" +
                                              "        public static final void main(String [] args) { // gotta love Java\n" +
                                              "            // Say hello\n" +
                                              "          System./*wait*/out./*for*/println/*it*/(\"Hello/*\");\n" +
                                              "        }\n" +
                                              "          \n" +
                                              "  }     ");
    }

    @AfterAll
    public void removeFilesHierarchy() throws IOException {
        FileUtils.deleteDirectory(new File("test"));
    }

    private void createFile(String fileName, String content) throws IOException {
        new File(fileName).getParentFile().mkdirs();
        File file = new File(fileName);
        FileWriter fileWriter = new FileWriter(file);
        fileWriter.write(content);
        fileWriter.flush();
        fileWriter.close();
    }

}
