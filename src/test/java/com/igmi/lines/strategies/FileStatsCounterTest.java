package com.igmi.lines.strategies;

import com.igmi.lines.service.FileStatsCounter;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;

import static java.util.Collections.emptyList;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class FileStatsCounterTest extends AbstractFileStatsTest {

    @Test
    public void testOneFileNoComments() throws FileNotFoundException {
        File rootFile = new File(FILE_NO_COMMENTS);
        FileStatsCounter fileStatsCounter = new FileStatsCounter(rootFile, new CodeLinesCountFileStatsStrategy());
        FileStatsCounter.FileStatsCounterResult process = fileStatsCounter.process();
        HashMap<String, FileStatsCounter.FileRecord> rawResults = process.getRawResults();

        assertEquals(1, rawResults.size());
        assertTrue(rawResults.containsKey(rootFile.getAbsolutePath()));

        FileStatsCounter.FileRecord fileRecord = rawResults.get(rootFile.getAbsolutePath());
        assertEquals(5, fileRecord.getCount());
        assertEquals(0, fileRecord.getDepth());
        assertEquals(emptyList(), fileRecord.getChild());
    }

    @Test
    public void testRootDirectory() throws FileNotFoundException {
        File rootFile = new File(ROOT_DIRECTORY);
        FileStatsCounter fileStatsCounter = new FileStatsCounter(rootFile, new CodeLinesCountFileStatsStrategy());
        FileStatsCounter.FileStatsCounterResult result = fileStatsCounter.process();
        HashMap<String, FileStatsCounter.FileRecord> rawResults = result.getRawResults();
        result.printResults();

        assertEquals(10, rawResults.size());
        assertTrue(rawResults.containsKey(rootFile.getAbsolutePath()));

        FileStatsCounter.FileRecord rootFileRecord = rawResults.get(rootFile.getAbsolutePath());
        assertEquals(20, rootFileRecord.getCount());
        assertEquals(0, rootFileRecord.getDepth());
        assertEquals(4, rootFileRecord.getChild().size());
    }
}
