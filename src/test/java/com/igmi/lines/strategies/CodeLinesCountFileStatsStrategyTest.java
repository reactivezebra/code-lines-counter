package com.igmi.lines.strategies;


import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.FileNotFoundException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class CodeLinesCountFileStatsStrategyTest extends AbstractFileStatsTest {

    private CodeLinesCountFileStatsStrategy getInstanceForTest() {
        return new CodeLinesCountFileStatsStrategy();
    }

    @Test
    public void testFileNotFound() {
        CodeLinesCountFileStatsStrategy codeLinesCountFileStatsStrategy = getInstanceForTest();
        assertThrows(FileNotFoundException.class, () -> {
            codeLinesCountFileStatsStrategy.countFileStats(new File("test/file6.java"));
        });
    }

    @Test
    public void testNoComments() throws FileNotFoundException {
        CodeLinesCountFileStatsStrategy codeLinesCountFileStatsStrategy = getInstanceForTest();
        int lines = codeLinesCountFileStatsStrategy.countFileStats(new File(FILE_NO_COMMENTS));
        assertEquals(5, lines);
    }

    @Test
    public void testOneSimpleComment() throws FileNotFoundException {
        CodeLinesCountFileStatsStrategy codeLinesCountFileStatsStrategy = getInstanceForTest();
        int lines = codeLinesCountFileStatsStrategy.countFileStats(new File(FILE_1_SIMPLE_COMMENT));
        assertEquals(3, lines);
    }

    @Test
    public void testMultilineComment() throws FileNotFoundException {
        CodeLinesCountFileStatsStrategy codeLinesCountFileStatsStrategy = getInstanceForTest();
        int lines = codeLinesCountFileStatsStrategy.countFileStats(new File(FILE_1_MULTILINE_COMMENT));
        assertEquals(4, lines);
    }

    @Test
    public void testComplexComments() throws FileNotFoundException {
        CodeLinesCountFileStatsStrategy codeLinesCountFileStatsStrategy = getInstanceForTest();
        int lines = codeLinesCountFileStatsStrategy.countFileStats(new File(FILE_COMPLEX_LINES));
        assertEquals(5, lines);
    }
}
