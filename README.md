# README #

##About:
Implementation of kata13: Counting Code Lines + aggregated stats for folders hierarchy

###Requirements
Java 1.8+
Maven

##Build
mvn clean install

##Run
java -jar code-lines-counter-1.0.0.jar

##Tests
mvn test

